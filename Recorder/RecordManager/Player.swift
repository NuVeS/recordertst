//
//  Player.swift
//  Recorder
//
//  Created by Максуд2 on 29.03.2018.
//  Copyright © 2018 Максуд. All rights reserved.
//

import AVKit

class Player: NSObject{
    private var audioPlayer: AVAudioPlayerNode! = AVAudioPlayerNode()
    private var inputStream: InputStream!
    private var audioEngine: AVAudioEngine! = AVAudioEngine()
    
    func start(){
        let resource = Bundle.main.path(forResource: "indila", ofType: "mp3")
        guard let path = resource else {
            return
        }
        inputStream = InputStream(fileAtPath: path)
        inputStream.schedule(in: RunLoop.current, forMode: .defaultRunLoopMode)
        var buffer = [UInt8](repeating: 0, count: 1024)
        
        while (inputStream.hasBytesAvailable) {
            let length = inputStream.read(&buffer, maxLength: buffer.count)
            
            if (length > 0) {
                
                if (audioEngine.isRunning) {
                    audioEngine.stop()
                    audioEngine.reset()
                }
                
                let audioBuffer = bytesToAudioBuffer(buffer)
                let mainMixer = audioEngine!.mainMixerNode
                
                audioEngine.connect(audioPlayer, to: mainMixer, format: audioBuffer.format)
                
                audioPlayer.scheduleBuffer(audioBuffer, completionHandler: nil)
                
                do {
                    try audioEngine!.start()
                }
                catch{
                    print("failed to start engine")
                }
                
                audioPlayer!.play()
            }
        }
    }
    
    private func bytesToAudioBuffer(_ buf: [UInt8]) -> AVAudioPCMBuffer {
        
        let fmt = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: 44100, channels: 1, interleaved: true)
        let frameLength = UInt32(buf.count) / (fmt?.streamDescription.pointee.mBytesPerFrame)!
        
        let audioBuffer = AVAudioPCMBuffer(pcmFormat: fmt!, frameCapacity: frameLength)
        audioBuffer?.frameLength = frameLength
        
        let dstLeft = audioBuffer?.floatChannelData![0]
        
        buf.withUnsafeBufferPointer {
            let src = UnsafeRawPointer($0.baseAddress!).bindMemory(to: Float.self, capacity: Int(frameLength))
            dstLeft?.initialize(from: src, count: Int(frameLength))
        }
        
        return audioBuffer!
    }
    
    func stop(){
        audioPlayer.stop()
        audioEngine.stop()
        Thread.current.cancel()
    }
}

extension Player: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if(!flag){
            stop()
        }
    }
}
