//
//  RecordManager.swift
//  Recorder
//
//  Created by Максуд2 on 29.03.2018.
//  Copyright © 2018 Максуд. All rights reserved.
//

import AVFoundation

class RecordManager: NSObject{
    private var audioSession: AVAudioSession!
    private var recorder = Recorder()
    private let player = Player()
    
    func start(){
        audioSession =  AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try audioSession.setActive(true)
            audioSession.requestRecordPermission() { [unowned self] allowed in
                if allowed {
                    self.recorder.start()
                } else {
                    print("no allowed record")
                }
            }
            player.start()
        } catch {
            print("failed init session")
        }
    }
    
    func stop(){
        player.stop()
        recorder.stop()
    }
}
