//
//  Recorder.swift
//  Recorder
//
//  Created by Максуд2 on 29.03.2018.
//  Copyright © 2018 Максуд. All rights reserved.
//

import AVKit

class Recorder: NSObject{
    var audioEngine: AVAudioEngine?
    
    func start() {
        self.audioEngine = AVAudioEngine()
        guard let engine = self.audioEngine else {
            print("error start engine")
            return
        }
        let input = engine.inputNode
        
        let format = input.inputFormat(forBus: 0)
        input.installTap(onBus: 0, bufferSize:4096, format:format, block: { buffer, when in
            if let channel1Buffer = buffer.floatChannelData?[0] {
                let arr = Array(UnsafeBufferPointer(start: channel1Buffer, count: Int(buffer.frameLength)))
                print("\(String(describing: arr))")
            }
        })
        
        engine.prepare()
        Thread.detachNewThread {
            do{
                try engine.start()
            }catch{
                print("failed start recording")
            }
        }
    }
    
    func stop() {
        audioEngine?.stop()
        audioEngine = nil
        Thread.current.cancel()
    }
}

extension Recorder: AVAudioRecorderDelegate{
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            stop()
        }
    }
}
