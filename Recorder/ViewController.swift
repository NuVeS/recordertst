//
//  ViewController.swift
//  Recorder
//
//  Created by Максуд2 on 29.03.2018.
//  Copyright © 2018 Максуд. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var manager = RecordManager()
    private var isRecording: Bool = false
    
    @IBAction func start(_ sender: Any) {
        if isRecording{
            manager.stop()
        }else{
            manager.start()
        }
        isRecording = !isRecording
    }
}

